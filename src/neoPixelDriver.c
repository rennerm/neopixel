/**
 ******************************************************************************
 * @file    neoPixelDriver.h
 * @author  rennerm
 * @version V1.0
 * @date    08.06.2016
 * @brief   neoPixelDriver header
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2016 ETH RSL rennerm </center></h2>
 *
 ******************************************************************************
 * @information
 * derived from
 * https://www.bitcraze.io/2014/04/neopixel-driver-for-the-crazyflies-stm32/
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <neoPixelDriver.h>
#include <string.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TIMING_ONE  164
#define TIMING_ZERO 72

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint16_t led_dma[2][32];

static TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
static TIM_OCInitTypeDef TIM_OCInitStructure;
static GPIO_InitTypeDef GPIO_InitStructure;
static DMA_InitTypeDef DMA_InitStructure;
static NVIC_InitTypeDef NVIC_InitStructure;

uint8_t allLedDone = 1;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void
neoPixelInit(void)
{
  uint16_t PrescalerValue;
  uint16_t PeriodValue;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
  RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOE, ENABLE);

    /* GPIOA Configuration: TIM1 Channel 1 as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init (GPIOE, &GPIO_InitStructure);

  GPIO_PinAFConfig (GPIOE, GPIO_PinSource9, GPIO_AF_TIM1);

  /* Compute the prescaler and period value */
  PrescalerValue = (uint16_t) 0;
  PeriodValue = (uint32_t) (SystemCoreClock/800000) - 1;
  /* Time base configuration */
  TIM_TimeBaseStructure.TIM_Period = PeriodValue; // 800kHz
  TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit (TIM1, &TIM_TimeBaseStructure);

  /* PWM1 Mode configuration: Channel1 */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 0;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC1Init (TIM1, &TIM_OCInitStructure);

  TIM_OC1PreloadConfig (TIM1, TIM_OCPreload_Enable);

  TIM_CtrlPWMOutputs (TIM1, ENABLE);           // enable Timer 1

  /* configure DMA */
  /* DMA clock enable */
  RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_DMA2, ENABLE);

  /* DMA2 Channel6 Config */
  DMA_DeInit (DMA2_Stream5);

  //Wait until disabled
  while(DMA_GetCmdStatus(DMA2_Stream5) == ENABLE)
  {}

  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &TIM1->CCR1; //TIM1_CCR1_Address;  // physical address of Timer 3 CCR1
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&led_dma[0];    // this is the buffer memory
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_BufferSize = sizeof(led_dma[0]);
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull ;
  DMA_InitStructure.DMA_Channel = DMA_Channel_6;
  DMA_Init (DMA2_Stream5, &DMA_InitStructure);

  DMA_DoubleBufferModeConfig(DMA2_Stream5, (uint32_t)&led_dma[1], DMA_Memory_0);
  DMA_DoubleBufferModeCmd(DMA2_Stream5, ENABLE);

  NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 9;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init (&NVIC_InitStructure);

  DMA_ITConfig(DMA2_Stream5, DMA_IT_TC, ENABLE);
  DMA_ClearITPendingBit(DMA2_Stream5, DMA_IT_TCIF5);

  /* TIM1 Update DMA Request enable */
  TIM_DMACmd (TIM1, TIM_DMA_Update, ENABLE);

}

static void
fillLed(uint16_t *buffer, uint8_t *color)
{
  int i;

  for(i=0; i<8; i++) // GREEN data
    {
      buffer[i] = ((color[1]<<i) & 0x0080) ? TIMING_ONE:TIMING_ZERO;
    }
  for(i=0; i<8; i++) // RED
    {
      buffer[8+i] = ((color[0]<<i) & 0x0080) ? TIMING_ONE:TIMING_ZERO;
    }
  for(i=0; i<8; i++) // BLUE
    {
      buffer[16+i] = ((color[2]<<i) & 0x0080) ? TIMING_ONE:TIMING_ZERO;
    }
  for(i=0; i<8; i++) // WHITE
    {
      buffer[24+i] = ((color[3]<<i) & 0x0080) ? TIMING_ONE:TIMING_ZERO;
    }
}

static int current_led = 0;
static int total_led = 0;
static uint8_t (*color_led)[4] = NULL;

void
neoPixelSend(uint8_t (*color)[4], uint16_t len)
{
  if (len < 1)
    return;

  //Wait for previous transfer to be finished
  while (!allLedDone)
    ;
  allLedDone = 0;

  // Set interrupt context ...
  current_led = 0;
  total_led = len;
  color_led = color;

  fillLed(led_dma[0], color_led[current_led]);

  current_led++;

  if (current_led<total_led)
      fillLed(led_dma[1], color_led[current_led]);
  else
      bzero(led_dma[1], sizeof(led_dma[0]));

  current_led++;

  DMA_SetCurrDataCounter(DMA2_Stream5, 32); // load number of bytes to be transferred

  TIM_SetCounter(TIM1, 0);
  TIM_SetCompare1(TIM1, 0);
  TIM_Cmd (TIM1, ENABLE);                      // Go!!!

  // Start DMA transfer after starting the timer. This prevents the
  // DMA/PWM from dropping the first bit.
  DMA_Cmd (DMA2_Stream5, ENABLE);       // enable DMA channel 2
}

void
neoPixelDmaIsr(void)
{

  uint16_t * bufferAddress;

  if (total_led == 0)
    {
      TIM_Cmd(TIM1, DISABLE);
      DMA_Cmd(DMA2_Stream5, DISABLE);
    }

  if (DMA_GetITStatus(DMA2_Stream5, DMA_IT_TCIF5))
    {
        DMA_ClearITPendingBit(DMA2_Stream5, DMA_IT_TCIF5);

        if (DMA_GetCurrentMemoryTarget(DMA2_Stream5) == 0)
          bufferAddress = led_dma[1];
        else
          bufferAddress = led_dma[0];

        if (current_led<total_led)
            fillLed(bufferAddress, color_led[current_led]);
        else
            bzero(bufferAddress, sizeof(led_dma[0]));

        current_led++;

        if (current_led >= total_led + 2)
          {
            allLedDone = 1;

            TIM_Cmd (TIM1, DISABLE);           // disable Timer 1
            DMA_Cmd (DMA2_Stream5, DISABLE);      // disable DMA channel 2

            total_led = 0;
          }
        else
          {
            allLedDone = 0;
          }
    }
}
