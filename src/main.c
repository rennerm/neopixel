/**
  ******************************************************************************
  * @file    main.c
  * @author  rennerm
  * @version V1.0
  * @date    08.06.2016
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 ETH RSL rennerm </center></h2>
  *
  ******************************************************************************
	* @information
	*
	*
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"


/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define RED {0xff, 0x00, 0x00, 0x00}
#define GREEN {0x00, 0xff, 0x00, 0x00}
#define BLUE {0x00, 0x00, 0xff, 0x00}
#define WHITE {0xff, 0xff, 0xff, 0xff}
#define BLACK {0x00, 0x00, 0x00, 0x00}
#define PINK {0xff, 0x00, 0x7f, 0x00}
#define LIGHTBLUE {0x00, 0x7f, 0xff, 0x1f}

#define NLED 24

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* System */
static __IO uint32_t uwTimingDelay;
RCC_ClocksTypeDef RCC_Clocks;

/* systick management */
extern volatile uint8_t run;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  static uint8_t ledBuffer[NLED][4];
  static uint8_t color[4] = RED;
  uint8_t firstRun = 1;

  /* SysTick end of count event */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config(SystemCoreClock / 1000);

	/* Configuration and Initialisation */
  RCC_configuration();
	GPIO_configuration();							// GPIO general
	neoPixelInit();

	generateSpinArray(ledBuffer, NLED, color, 10, CCW);

  /* Infinite loop */
	while(1)
	{
	    if(run)
	      {
	        run = 0;

	        if(!firstRun)
	          spinEffect(ledBuffer, NLED, CCW);

          neoPixelSend(ledBuffer, NLED);
          GPIO_ToggleBits(GPIOD, GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15);

          firstRun = 0;
	      }
	}
}

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{
  uwTimingDelay = nTime;

  while(uwTimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (uwTimingDelay != 0x00)
  {
    uwTimingDelay--;
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
