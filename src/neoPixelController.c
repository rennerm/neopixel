/**
  ******************************************************************************
  * @file    neoPixelDriver.h
  * @author  rennerm
  * @version V1.0
  * @date    08.06.2016
  * @brief   neoPixelDriver header
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 ETH RSL rennerm </center></h2>
  *
  ******************************************************************************
  * @information
  * derived from
  * https://www.bitcraze.io/2014/04/neopixel-driver-for-the-crazyflies-stm32/
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <neoPixelController.h>
#include <neoPixelDriver.h>
#include <math.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define COPY_COLOR(dest, orig) dest[0]=orig[0]; dest[1]=orig[1]; dest[2]=orig[2]; dest[3]=orig[3]

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void logarithmicallyDecreasing(uint32_t nrLEDs,uint8_t color[4], uint8_t buffer[4], direction_type direction);
/* Private functions ---------------------------------------------------------*/


/**************** Black (LEDs OFF) ***************/

void blackEffect(uint8_t buffer[][4], uint32_t nrLEDs)
{
  int i;

  for (i=0; i<nrLEDs; i++)
  {
    buffer[i][0] = 0;
    buffer[i][1] = 0;
    buffer[i][2] = 0;
    buffer[i][3] = 0;
  }
}

void spinEffect(uint8_t buffer[][4], uint32_t nrLEDs, direction_type direction)
{
  int i;
  uint8_t temp[4];

  if(direction == CW)
    {
      COPY_COLOR(temp,buffer[nrLEDs-1]);
      for (i=nrLEDs-1; i>0; i--)
      {
        COPY_COLOR(buffer[i], buffer[i-1]);
      }
      COPY_COLOR(buffer[0],temp);
    }
  else
    {
      COPY_COLOR(temp,buffer[0]);
      for (i=0; i<nrLEDs-1; i++)
        {
          COPY_COLOR(buffer[i], buffer[i+1]);
        }
      COPY_COLOR(buffer[nrLEDs-1],temp);
    }
}

void generateSpinArray(uint8_t buffer[][4], uint32_t nrLEDs, uint8_t color[4], uint32_t tailLength, direction_type direction)
{
  int i;

  if(direction == CW)
    for (i=0; i<nrLEDs; i++)
      logarithmicallyDecreasing(tailLength, color, buffer[i], direction);
  else
    for (i=nrLEDs-1; i>=0; i--)
      logarithmicallyDecreasing(tailLength, color, buffer[i], direction);

}

void logarithmicallyDecreasing(uint32_t nrLEDs,uint8_t color[4], uint8_t buffer[4], direction_type direction)
{
  static uint32_t cLED = 1;
  float scale;

  nrLEDs++; //calculate one more as the last one is 0

  if(cLED < nrLEDs)
    scale = 1-logf(nrLEDs-cLED)/logf(nrLEDs);
  else
    scale = 0;

  buffer[0] = color[0]*scale;
  buffer[1] = color[1]*scale;
  buffer[2] = color[2]*scale;
  buffer[3] = color[3]*scale;

  cLED++;
}
