/**
  ******************************************************************************
  * @file    main.h
  * @author  rennerm
  * @version V1.0
  * @date    08.06.2016
  * @brief   Main program body header
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 ETH RSL rennerm </center></h2>
  *
  ******************************************************************************
  * @information
  *
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "configuration.h"
#include "neoPixelController.h"
#include "neoPixelDriver.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void TimingDelay_Decrement(void);
void Delay(__IO uint32_t nTime);



#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
