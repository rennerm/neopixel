/**
  ******************************************************************************
  * @file    neoPixelDriver.c
  * @author  rennerm
  * @version V1.0
  * @date    08.06.2016
  * @brief   neoPixelDriver
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 ETH RSL rennerm </center></h2>
  *
  ******************************************************************************
  * @information
  * derived from
  * https://www.bitcraze.io/2014/04/neopixel-driver-for-the-crazyflies-stm32/
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NEOPIXELDRIVER_H__
#define __NEOPIXELDRIVER_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void neoPixelInit(void);
void neoPixelSend(uint8_t (*color)[4], uint16_t len);
void neoPixelDmaIsr(void);

#endif
