/**
  ******************************************************************************
  * @file    neoPixelController.c
  * @author  rennerm
  * @version V1.0
  * @date    08.06.2016
  * @brief   neoPixelDriver
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 ETH RSL rennerm </center></h2>
  *
  ******************************************************************************
  * @information
  * derived from
  * https://www.bitcraze.io/2014/04/neopixel-driver-for-the-crazyflies-stm32/
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NEOPIXELCONTROLLER_H__
#define __NEOPIXELCONTROLLER_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/* Exported types ------------------------------------------------------------*/
typedef enum
{
  CW,
  CCW
} direction_type;
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void blackEffect(uint8_t buffer[][4], uint32_t quantity);
void spinEffect(uint8_t buffer[][4], uint32_t quantity, direction_type direction);
void generateSpinArray(uint8_t buffer[][4], uint32_t quantity, uint8_t color[4], uint32_t tailLength, direction_type direction);

#endif
