/**
  ******************************************************************************
  * @file     configuration.h
	* @autor		rnm1
	* @version	V1.0
	* @date			29.02.2016
  * @brief    library file configuration.c
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONFIGURATION_H
#define __CONFIGURATION_H

/* Includes ------------------------------------------------------------------*/	
#include "stm32f4xx.h"

/* Private defines------------------------------------------------------------*/
/* Prototypes ------------------------------------------------------------------*/
void RCC_configuration(void);
void GPIO_configuration(void);

#endif /*__CONFIGURATION_H */

/********** (C) COPYRIGHT BFH TI Bienne - Mechatronics Lab *****END OF FILE****/
