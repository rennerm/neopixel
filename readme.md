# NeoPixel Driver for STM32F4(Discovery) V1.0.1  
Copyright (C) 2016  Matthias Renner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## 1. Introduction

This is a subproject of the STM32F4_OpenSTM32 project ([link](https://bitbucket.org/rennerm/stm32f4_openstm32/overview)). Its goal is to drive the NeoPixel **RGBW** LEDs on the STM32F4 Discovery board with DMA (double buffering).

The project is depending on OpenSTM32 plug-in and Eclipse (for installation instructions go [here](http://www.openstm32.org/Installing+System+Workbench+for+STM32+from+Eclipse?structure=Documentation)).

Also, this project is build up on the STM32F4xx standard peripherals library which is not included in this repository. How to set up this project with all dependencies will be described in section [2](#markdown-header-2.-instructions).

*neoPixelController.c* and *neoPixelDriver.c* are derived form the neoPixel driver for Crazyflie (for more infos and the original source code look [here](https://www.bitcraze.io/2014/04/neopixel-driver-for-the-crazyflies-stm32/)).

## 2. Instructions

1. Set up Eclipse with OpenSTM32 plug-in ([link](http://www.openstm32.org/Installing+System+Workbench+for+STM32+from+Eclipse?structure=Documentation))
2. go to your workbench and clone the parent project of this subproject:

    * for parent project with all its subprojects:

            git clone --recursive git@bitbucket.org:rennerm/stm32f4_openstm32.git

    * only parrent project with NeoPixel subproject:

            git clone git@bitbucket.org:rennerm/stm32f4_openstm32.git
            cd stm32f4_openstm32
            git submodule update --init -- Projects/neopixel

3. in Eclipse select: *File --> Import --> General --> Existing Projects into Workspace*
4. Browse to *stm32f4_openstm32/Libraries/stm32f4discovery_stdperiph_lib* and import it
5. repeat step 3 and 4 for *stm32f4_openstm32/Projects/neopixel*
6. Build it
